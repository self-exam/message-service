package uz.developers.message.service.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.developers.message.service.domain.MessageKey;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MessageKeyRepository extends JpaRepository<MessageKey, UUID> {
    Optional<MessageKey> findByName(String key);
}
