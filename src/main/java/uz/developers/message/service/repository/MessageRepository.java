package uz.developers.message.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.developers.message.service.domain.Language;
import uz.developers.message.service.domain.Message;
import uz.developers.message.service.domain.MessageKey;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MessageRepository extends JpaRepository<Message, UUID> {
    Optional<Message> findByKeyAndLanguage(MessageKey message, Language language);
}
