package uz.developers.message.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.developers.message.service.dto.*;
import uz.developers.message.service.service.MessageKeyService;

import java.util.UUID;

@RestController
@RequestMapping("/v1/message-keys")
public class MessageKeyController {
    private final MessageKeyService messageKeyService;

    public MessageKeyController(MessageKeyService messageKeyService) {
        this.messageKeyService = messageKeyService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void save(@RequestBody SaveMessageKeyRequest messageKey) {
        messageKeyService.save(messageKey);
    }

    @PutMapping("/{keyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable UUID keyId, @RequestBody UpdateMessageKeyRequest messageKey) {
        messageKeyService.update(keyId, messageKey);
    }

    @GetMapping
    public ResponseEntity<PageAbleResponse<FindMessageKeyResponse>> find(PageAbleRequest pageAbleRequest) {
        return ResponseEntity.ok(messageKeyService.find(pageAbleRequest));
    }
}
