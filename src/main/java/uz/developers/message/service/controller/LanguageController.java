package uz.developers.message.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.developers.message.service.dto.BaseResponse;
import uz.developers.message.service.dto.FindLanguageResponse;
import uz.developers.message.service.dto.SaveLanguageRequest;
import uz.developers.message.service.service.LanguageService;

@RestController
@RequestMapping("/v1/languages")
public class LanguageController {
    private final LanguageService languageService;

    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void save(@RequestBody SaveLanguageRequest language) {
        languageService.save(language);
    }

    @GetMapping
    public ResponseEntity<BaseResponse<FindLanguageResponse>> find() {
        return ResponseEntity.ok(languageService.find());
    }
}
