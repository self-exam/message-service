package uz.developers.message.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.developers.message.service.dto.*;
import uz.developers.message.service.service.MessageService;

@RestController
@RequestMapping("/v1/messages")
public class MessageController {
    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/find-by-keys")
    public ResponseEntity<BaseResponse<MessageResponse>> getAllMessages(@RequestBody MessageRequest messageRequest) {
        return ResponseEntity.ok(messageService.getMessages(messageRequest));
    }

    @GetMapping("/find-by-key/{messageKey}")
    public ResponseEntity<BaseResponse<FindMessageByKeyResponse>> find(@PathVariable String messageKey) {
        return ResponseEntity.ok(messageService.findMessages(messageKey));
    }

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateMessage(@RequestBody UpdateMessageRequest messageRequest) {
        messageService.update(messageRequest);
    }
}
