package uz.developers.message.service.common;

import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class Utils {
    public static String getLanguage(){
        Locale locale = LocaleContextHolder.getLocale();
        return locale.toLanguageTag();
    }
}
