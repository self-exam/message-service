package uz.developers.message.service.exception;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import uz.developers.message.service.dto.ErrorBaseResponse;

import java.util.List;

@ControllerAdvice
public class Handler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorBaseResponse> exception(Exception e) {
        return ResponseEntity.status(HttpStatusCode.valueOf(400))
                .body(ErrorBaseResponse.builder()
                        .code(4000)
                        .message(e.getMessage())
                        .details(List.of())
                        .build());
    }
}
