package uz.developers.message.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.developers.message.service.domain.Message;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FindMessageByKeyResponse {
    private UUID id;
    private String language;
    private String message;
    private String key;

    public FindMessageByKeyResponse(Message message){
        this.id = message.getId();
        this.language = message.getLanguage().getName();
        this.key = message.getKey().getName();
        this.message = message.getMessage();
    }
}
