package uz.developers.message.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageAbleRequest {
    private Integer page;
    private Integer size;

    public int getPage() {
        return Math.max(Objects.requireNonNullElse(page, 0), 0);
    }

    public int getSize() {
        return Math.min(Math.max(Objects.requireNonNullElse(size, 10), 1), 100);
    }
}
