package uz.developers.message.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.developers.message.service.domain.MessageKey;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FindMessageKeyResponse {
    private UUID id;
    private String key;
    private Integer code;

    public FindMessageKeyResponse(MessageKey messageKey){
        this.id = messageKey.getId();
        this.key = messageKey.getName();
        this.code = messageKey.getCode();
    }
}
