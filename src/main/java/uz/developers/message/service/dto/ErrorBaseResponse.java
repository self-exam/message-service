package uz.developers.message.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorBaseResponse {
    private Integer code;
    private String message;
    private List<Param> details;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Param {
        private String key;
        private String value;
    }
}
