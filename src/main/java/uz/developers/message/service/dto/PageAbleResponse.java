package uz.developers.message.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageAbleResponse<T> {
    private Integer page;
    private Integer size;
    private Long totalElements;
    private Collection<T> data;
}
