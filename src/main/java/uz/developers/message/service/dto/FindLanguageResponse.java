package uz.developers.message.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.developers.message.service.domain.Language;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FindLanguageResponse {
    private UUID id;
    private String language;

    public FindLanguageResponse(Language language){
        this.id = language.getId();
        this.language = language.getName();
    }
}
