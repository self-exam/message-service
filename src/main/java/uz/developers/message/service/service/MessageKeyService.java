package uz.developers.message.service.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import uz.developers.message.service.domain.MessageKey;
import uz.developers.message.service.dto.*;
import uz.developers.message.service.repository.MessageKeyRepository;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class MessageKeyService {
    private final MessageKeyRepository messageKeyRepository;

    public MessageKeyService(MessageKeyRepository messageKeyRepository) {
        this.messageKeyRepository = messageKeyRepository;
    }

    public void save(SaveMessageKeyRequest request) {
        Optional<MessageKey> optionalMessageKey = messageKeyRepository.findByName(request.getKey());

        if (optionalMessageKey.isPresent()) {
            throw new RuntimeException("this key already exists");
        } else {
            messageKeyRepository.save(MessageKey
                    .builder()
                    .code(request.getCode())
                    .name(request.getKey())
                    .build());
        }
    }

    public void update(UUID messageKeyId, UpdateMessageKeyRequest request) {
        MessageKey messageKey = messageKeyRepository.findById(messageKeyId).orElseThrow(() -> new RuntimeException("this message key does not exist"));

        if (Objects.isNull(request.getCode())) throw new RuntimeException("code must not be null");

        messageKey.setCode(request.getCode());

        messageKeyRepository.save(messageKey);
    }

    public PageAbleResponse<FindMessageKeyResponse> find(PageAbleRequest pageAbleRequest) {
        Page<MessageKey> messageKeys = messageKeyRepository.findAll(PageRequest.of(pageAbleRequest.getPage(), pageAbleRequest.getSize()));

        return PageAbleResponse.<FindMessageKeyResponse>builder()
                .data(messageKeys.get()
                        .map(FindMessageKeyResponse::new)
                        .toList())
                .page(messageKeys.getNumber())
                .size(messageKeys.getSize())
                .totalElements(messageKeys.getTotalElements())
                .build();
    }

}
