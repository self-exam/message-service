package uz.developers.message.service.service;

import org.springframework.stereotype.Service;
import uz.developers.message.service.common.Utils;
import uz.developers.message.service.config.ApplicationProperties;
import uz.developers.message.service.domain.Language;
import uz.developers.message.service.domain.Message;
import uz.developers.message.service.domain.MessageKey;
import uz.developers.message.service.dto.*;
import uz.developers.message.service.repository.LanguageRepository;
import uz.developers.message.service.repository.MessageKeyRepository;
import uz.developers.message.service.repository.MessageRepository;

import java.util.Optional;

@Service
public class MessageService {
    private final ApplicationProperties applicationProperties;
    private final MessageRepository messageRepository;
    private final MessageKeyRepository messageKeyRepository;
    private final LanguageRepository languageRepository;

    public MessageService(ApplicationProperties applicationProperties, MessageRepository messageRepository, MessageKeyRepository messageKeyRepository, LanguageRepository languageRepository) {
        this.applicationProperties = applicationProperties;
        this.messageRepository = messageRepository;
        this.messageKeyRepository = messageKeyRepository;
        this.languageRepository = languageRepository;
    }

    public BaseResponse<MessageResponse> getMessages(MessageRequest messageRequest) {

        String languageName = Utils.getLanguage();

        Optional<Language> optionalLanguage = languageRepository.findByName(languageName);

        Language language = optionalLanguage.orElseGet(() -> languageRepository.findByName(applicationProperties.getLanguage())
                .orElseThrow());

        return new BaseResponse<>(messageRequest
                .getData()
                .stream()
                .map(MessageRequest.Key::getKey)
                .map(key -> messageKeyRepository.findByName(key)
                        .map(messageKey -> messageRepository
                                .findByKeyAndLanguage(messageKey, language)
                                .map(message ->
                                        new MessageResponse(message.getMessage(), message.getKey().getCode(), message.getKey().getName()))
                                .orElse(new MessageResponse(key, applicationProperties.getCode(), key)))
                        .orElse(new MessageResponse(key, applicationProperties.getCode(), key))).toList());
    }

    public BaseResponse<FindMessageByKeyResponse> findMessages(String messageKey) {
        MessageKey key = messageKeyRepository.findByName(messageKey).orElseThrow(() -> new RuntimeException("messageKey not found"));

        return new BaseResponse<>(languageRepository
                .findAll()
                .stream()
                .map(language ->
                        messageRepository.findByKeyAndLanguage(key, language).map(FindMessageByKeyResponse::new)
                                .orElse(FindMessageByKeyResponse.builder()
                                        .key(key.getName())
                                        .language(language.getName())
                                        .build())
                ).toList());
    }

    public void update(UpdateMessageRequest request){
        request
                .getData()
                .forEach(message -> {
                    Optional<MessageKey> optionalMessageKey = messageKeyRepository.findByName(message.getKey());
                    if (optionalMessageKey.isEmpty()) return;
                    MessageKey messageKey = optionalMessageKey.get();

                    Optional<Language> optionalLanguage = languageRepository.findByName(message.getLanguage());
                    if (optionalLanguage.isEmpty()) return;
                    Language language = optionalLanguage.get();

                    messageRepository.save(
                            Message.builder()
                                    .id(message.getId())
                                    .message(message.getMessage())
                                    .language(language)
                                    .key(messageKey)
                                    .build()
                    );
                });
    }
}
