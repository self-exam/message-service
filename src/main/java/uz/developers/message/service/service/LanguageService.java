package uz.developers.message.service.service;

import org.springframework.stereotype.Service;
import uz.developers.message.service.domain.Language;
import uz.developers.message.service.dto.BaseResponse;
import uz.developers.message.service.dto.FindLanguageResponse;
import uz.developers.message.service.dto.SaveLanguageRequest;
import uz.developers.message.service.repository.LanguageRepository;

import java.util.List;

@Service
public class LanguageService {
    private final LanguageRepository languageRepository;

    public LanguageService(LanguageRepository languageRepository) {
        this.languageRepository = languageRepository;
    }

    public void save(SaveLanguageRequest request) {
        languageRepository.findByName(request.getLanguage()).ifPresent(language -> {
            throw new RuntimeException("this language already exists");
        });

        languageRepository.save(Language.builder()
                        .name(request.getLanguage())
                .build());
    }

    public BaseResponse<FindLanguageResponse> find(){
        return new BaseResponse<>(languageRepository
                .findAll()
                .stream()
                .map(FindLanguageResponse::new)
                .toList());
    }
}
