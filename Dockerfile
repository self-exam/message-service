FROM openjdk:21-ea-17-slim-buster
WORKDIR /app
COPY target/*.jar /app/spring-boot-application.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar","/app/spring-boot-application.jar"]